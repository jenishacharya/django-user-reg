from django.urls import path
from basic_app import views

url_patterns = [
    path('user_list/', views.users_list, name='user_list')
]