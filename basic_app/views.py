from django.shortcuts import render
from basic_app.models import User
from basic_app.forms import FormUser
# Create your views here.

def index(request):
    return render(request, 'basic_app/index.html')

def users_list(request):
    user_list = User.objects.order_by('first_name')
    user_dictionary = {'users': user_list}
    return render(request, 'basic_app/users.html', context=user_dictionary)

def new_user(request):
    form = FormUser()
    if request.method == 'POST':
        form = FormUser(request.POST)

        if form.is_valid():
            form.save()
            return index(request)
        else:
            print('Error! form invalid')
    return render(request, 'basic_app/form_page.html', {'form': form})

