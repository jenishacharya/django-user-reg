from django import forms
from basic_app.models import User

class FormUser(forms.ModelForm):
    class Meta():
        model = User
        fields = '__all__'
        # fields = ("first_name", "last_name")
        # exclude = ["email"]